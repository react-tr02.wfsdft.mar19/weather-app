import React, {Component} from 'react';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import { Grid, Row, Col} from 'react-flexbox-grid';
import './App.css';
import LocationList from "./components/LocationList";
import ForecastExtended from './components/ForecastExtended';

const cities = [
    'Madrid,es',
    'Sevilla,es',
    'Roma,it',
    'Lima,pe'
];

class App extends Component{

    constructor() {
        super();
        this.state = {
            city:  null,
        }
    };

    handleSelectedLocation = city => {
        console.log(`handleWeatherLocationClick ${city}`);
        this.setState({city})

    };

    render() {
        const { city } = this.state;
        return (
            <MuiThemeProvider>
                <Grid>
                    <Row>
                        <AppBar position="sticky">
                            <Toolbar>
                                <Typography variant="title" color="inherit">
                                    Meteo Alejo App
                                </Typography>
                            </Toolbar>
                        </AppBar>
                    </Row>

                    <Row>
                        <Col xs={12} md={6}>
                            <LocationList
                                cities={cities}
                                onSelectedLocation={this.handleSelectedLocation}/>
                        </Col>

                        <Col xs={12} md={6}>
                            <Paper elevation={4}>
                                <div className="detalles">
                                    {city &&
                                        <ForecastExtended city={city}/>
                                    }
                                </div>
                            </Paper>
                        </Col>
                    </Row>

                </Grid>
            </MuiThemeProvider>
        );
    }
}

export default App;
