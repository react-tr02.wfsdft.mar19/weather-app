import {api_key, url_api} from "../constants/api_urls";

const getUrlWeatherbyCity = city => {
    return `${url_api}?q=${city}&appid=${api_key}`;
}

export default getUrlWeatherbyCity;
