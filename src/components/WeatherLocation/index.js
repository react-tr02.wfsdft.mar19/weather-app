import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import  PropTypes  from 'prop-types';
import Location from './Location';
import WeatherData from './WeatherData';
import transformWeather from './../../services/transformWeather';
import getUrlWeatherbyCity from './../../services/getUrlWeatherbyCity';
import './styles.css';


class WeatherLocation extends Component {

    constructor(props) {
        super(props);
        const { city } = props;
        this.state = {
            city,
            data: null,
        };
        console.log("constructor");
    }

    componentDidMount() {
        console.log("componentDidMount");
        this.handleUpdateClick();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("componentDidUpdate");
    }

    handleUpdateClick  = () => {
        const api_weather = getUrlWeatherbyCity(this.state.city);
        fetch(api_weather)
            .then(resolve => {return resolve.json()})
            .then(data => {
                const newWeather = transformWeather(data);
                this.setState({
                    data: newWeather,
                })
                console.log(data);
            })
        ;
        console.log("actualizado");
    };

    render () {
        const { onWeatherLocationClick } = this.props;
        const { city, data } = this.state;
    return (
    <div className="weather-location-cont" onClick={onWeatherLocationClick}>
        <Location city={city}/>
        {data ?
            <WeatherData data={data}/> :
            <CircularProgress />
        }
        {/*<button onClick={this.handleUpdateClick}>Actualizar</button>*/}
    </div>
    )
  }
}

WeatherLocation.propTypes = {
    city: PropTypes.string.isRequired,
    handleWeatherLocationClick: PropTypes.func,
}

export default WeatherLocation;
