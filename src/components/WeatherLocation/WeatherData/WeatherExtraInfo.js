import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

function WeatherExtraInfo ({ humidity,wind })  {
    return (
        <div className="weather-extrainfo-cont">
            <span className="extra-info-text">{`Humedad: ${humidity} %`}</span>
            <span className="extra-info-text">{`Viento: ${wind} `}</span>
        </div>
    )
}

WeatherExtraInfo.propTypes = {
    humidity: PropTypes.number.isRequired,
    wind: PropTypes.string.isRequired,
};

export default WeatherExtraInfo;
