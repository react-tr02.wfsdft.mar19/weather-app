import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ForecastItem from './ForecastItem';
import './styles.css';
import transformForecast from './../services/transformForecast';

// const days = [
//     'Lunes',
//     'Martes',
//     'Miercoles',
//     'Jueves',
//     'Viernes',
//     'Sabado',
//     'Domingo'
// ];
//
// const data = {
//     temperature: 18,
//     humidity: 10,
//     weatherState: 'normal',
//     wind: 5,
// };

export const api_key = "6c42f906073d6bc41767ef8dadad8752";
export const url_api_forc = "http://api.openweathermap.org/data/2.5/forecast";

class ForecastExtended extends Component {


    constructor(props) {
        super(props);
        this.state = { forecastData: null}
    }

    componentDidMount() {
        this.updateCity(this.props.city);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.city != this.props.city){
            this.setState({ forecastData: null });
            this.updateCity(nextProps.city);
        }
    }

    updateCity = city => {
        console.log("componentDidMount - Forecast Extended");
        const url_forecast = `${url_api_forc}?q=${city}&appid=${api_key}`;

        fetch(url_forecast)
            .then(data => {return data.json()})
            .then(weather_data => {
                // console.log(weather_data);
                const forecastData = transformForecast(weather_data);
                console.log(forecastData);
                this.setState({ forecastData });
            })
        ;
    }

    renderForecastItemDays= (forecastData) => {
        // return "Render Items";
        return (forecastData.map( forecast =>
            <ForecastItem key={`${forecast.weekDay}${forecast.hour}`}
                          weekDay={forecast.weekDay}
                          hour={forecast.hour}
                          data={forecast.data}
            />))
    }

    renderProgress = () => {
        return (<h3>Cargando pronóstico extendido</h3>);
    }

    render() {
        const { city } = this.props;
        const {forecastData } = this.state;
        return (<div>
            <h2 className="h2-forecast"> Pronostico extendido {city} </h2>
            {forecastData ?
                this.renderForecastItemDays(forecastData) :
                this.renderProgress()
            }
        </div>)
    };
}
ForecastExtended.propTypes = {
    city: PropTypes.string.isRequired,
};

export default ForecastExtended;
